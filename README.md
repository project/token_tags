# Token tags

Transforms token syntax into tags.

Example: `42 m[sup:2] apartment`

## Use case

When markup exceptionally needs to be used in plain text fields (title, ...)
and we prefer to use the token syntax to reduce errors
(like not matching tags e.g. <sup></sub>)
or be able to validate plain text input.

## Configuration

Make use of the `Token tags` field formatter
or in Twig: `{{ my_field | token_tags | raw }}`

## Roadmap

- Alter forms to include token browser and implement `hook_token_info`
- Handle attributes
- Add configuration to limit the tags that can be used,
 with a default configuration
