<?php

namespace Drupal\token_tags;

/**
 * Twig extension that triggers token replace as a filter.
 */
class TwigExtension extends \Twig_Extension {

  /**
   * {@inheritdoc}
   */
  public function getFilters() {
    return [
      new \Twig_SimpleFilter('token_tags', [$this, 'tokenReplace']),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getName() {
    return 'token_tags';
  }

  /**
   * Replaces all tokens in a given string with appropriate values.
   *
   * @param string $text
   *   An HTML string containing replaceable tokens.
   *
   * @return string
   *   The entered HTML text with tokens replaced.
   */
  public function tokenReplace($text) {
    return \Drupal::token()->replace($text);
  }

}
